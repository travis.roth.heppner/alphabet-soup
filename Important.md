### Important
It should be noted that there is an error in the challenge writeup.

The coordinates for the samples are reversed.  For example:
```
GOOD 4:0 4:3
```
## This means either of the following cases
The output is in a nonstandard form y,x:
```
WORD Y1:X1 Y2:X2
```
With the conventional coordinate system as follows.

|     | x = 0 | x = 1  | x = 2 | x = 3 | x = 4 |
|:---:|:-----:|:------:|:-----:|:-----:|:-----:|
| y=0 |   H   |   A    |   S   |   D   |   F   |
| y=1 |   G   |   E    |   Y   |   B   |  H    |
| y=2 |   J   |   K    |   L   |   Z   |   X   |
| y=3 |   C   |   V    |   B   |   L   |   N   |
| y=4 |  [G]  |  [O]   |  [O]  |  [D]  |   O   |

### Or

The output is in the standard x,y form:
```
WORD X1:Y1 X2:Y2
```
With an unconventional coordinate system as follows.

|     | y = 0 | y = 1 | y = 2 | y = 3 | y = 4 |
|:---:|:-----:|:-----:|:-----:|:-----:|:-----:|
| x=0 |   H   |   A   |   S   |   D   |   F   |
| x=1 |   G   |   E   |   Y   |   B   |   H   |
| x=2 |   J   |   K   |   L   |   Z   |   X   |
| x=3 |   C   |   V   |   B   |   L   |   N   |
| x=4 |  [G]  |  [O]  |  [O]  |  [D]  |   O   |



Either assumption seems wrong, but without clarification I believe the most correct approach would be to use standard coordinate/coordinate-systems.
I've made note of this in the Readme.md