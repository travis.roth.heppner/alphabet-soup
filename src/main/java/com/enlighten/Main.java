package com.enlighten;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;


public class Main {
    public static Integer[][] Directions = {
            {0, 1},
            {0, -1},
            {1, 0},
            {-1, 0},
            {1, 1},
            {1, -1},
            {-1, 1},
            {-1, -1},
    };

    public static void main(String[] args) {
        String filename;
        if (args.length != 1) {
            System.out.println("Unexpected input.  \nUsage:\n> alphabetSoup sample01.txt");
            System.exit(1);
        }
        filename = args[0];

        ArrayList<String> lines = new ArrayList<>();
        try {
            File file = new File(filename);
            Scanner reader = new Scanner(file);
            while (reader.hasNextLine()) {
                lines.add(reader.nextLine());
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Error occured while reading file. Check " + filename);
            e.printStackTrace();
        }

        String[] xy = lines.get(0).split("x");
        int maxX = Integer.parseInt(xy[0]);
        int maxY = Integer.parseInt(xy[1]);

        String[][] grid = new String[maxY][maxX];

        for (int y = 1; y <= maxY; y++) {
            grid[y - 1] = lines.get(y).split(" ");
        }

        List<String> words = lines.subList(maxY + 1, lines.size());

        ArrayList<String> results = searchForWords(maxX, maxY, grid, words);

        for (String result : results) {
            System.out.println(result);
        }
    }


    public static ArrayList<String> searchForWords(int maxX, int maxY, String[][] grid, List<String> words) {
        ArrayList<String> wordLocations = new ArrayList<>();
        ArrayList<String> locations;
        for (String word : words) {
            locations = searchForWord(grid, word, maxX, maxY);
            locations.forEach((location) -> wordLocations.add(word + " " + location));
        }
        return wordLocations;
    }


    private static ArrayList<String> searchForWord(String[][] grid, String word, int maxX, int maxY) {
        ArrayList<String> wordLocations = new ArrayList<>();
        word = word.replaceAll(" ", "");
        String firstChar = word.substring(0, 1);
        for (int y = 0; y < maxY; y++) {
            for (int x = 0; x < maxX; x++) {
                if (grid[y][x].equals(firstChar)) {
                    wordLocations.addAll(checkDirections(grid, word, maxX, maxY, x, y));
                }
            }
        }
        return wordLocations;
    }


    public static ArrayList<String> checkDirections(String[][] grid, String word, int maxX, int maxY, int x, int y) {
        ArrayList<String> locations = new ArrayList<>();
        for (Integer[] direction : Directions) {
            int dx = direction[0];
            int dy = direction[1];
            int endX = x + (dx * (word.length() - 1));
            int endY = y + (dy * (word.length() - 1));
            if (0 > endX || 0 > endY || endX >= maxX || endY >= maxY) {
                continue;
            }
            if (checkWord(grid, word, x, y, dx, dy)) {
                locations.add(String.format("%d:%d %d:%d", x, y, endX, endY));
            }
        }
        return locations;
    }

    public static boolean checkWord(String[][] grid, String word, int x, int y, int dx, int dy) {
        for (int index = 1; index < word.length(); index++) {
            int currentX = x + (dx * index);
            int currentY = y + (dy * index);
            String wordLetter = word.substring(index, index + 1);
            String gridLetter = grid[currentY][currentX];
            if (!wordLetter.equals(gridLetter)) {
                return false;
            }
        }
        return true;
    }
}
